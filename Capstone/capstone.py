from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass

class Employee(Person):
    def __init__(self, fname, lname, email, dept):
        super().__init__()
        self._fname = fname
        self._lname = lname
        self._email = email
        self._dept = dept

    def getFullName(self):
        return f'{self._fname} {self._lname}'

    def addRequest(self):
        return f'Request has been added'

    def checkRequest(self):
        print(f'Checking request')

    def addUser(self):
        return f'User has been added'

    def login(self):
        return f'{self._email} has logged in'

    def logout(self):
        return f'{self._email} has logged out'

class TeamLead(Person):
    def __init__(self, fname, lname, email, dept):
        super().__init__()
        self._fname = fname
        self._lname = lname
        self._email = email
        self._dept = dept
        self._members = []

    def getFullName(self):
        return f'{self._fname} {self._lname}'

    def checkRequest(self):
        print(f'Checking request')

    def addRequest(self):
        return f'Request has been added'

    def addUser(self):
        return f'User has been added'

    def login(self):
        return f'{self._email} has logged in'

    def logout(self):
        return f'{self._email} has logged out'

    def addMember(self, fname):
        self._members.append(fname)
        self._fname = fname

    def getMember(self):
        return self._members

class Admin(Person):
    def __init__(self, fname, lname, email, dept):
        super().__init__()
        self._fname = fname
        self._lname = lname
        self._email = email
        self._dept = dept

    def getFullName(self):
        return f'{self._fname} {self._lname}'

    def addRequest(self):
        return f'Request has been added'

    def checkRequest(self):
        print('Checking request')

    def login(self):
        return f'{self._email} has logged in'

    def logout(self):
        return f'{self._email} has logged out'

    def addUser(self):
        return f'User has been added'

class Request:
    def __init__(self, name, requester, date_requested):
        super().__init__()
        self._name = name
        self._requester = requester
        self._date_requested = date_requested
        self._status = ''

    def update_request(self):
        print('Request updated')

    def close_request(self):
        print('Request closed')

    def cancel_request(self):
        print('Request cancelled')

    def getFullName(self):
        return f'{self._fname} {self._lname}'

    def addRequest(self):
        return f'Request has been added'

    def checkRequest(self):
        print('Checking request')

    def set_status(self, status):
        self._status = status

    def closeRequest(self):
        return f'Request {self._name} has been closed'

    def addUser(self):
        return f'User has been added'

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == 'John Doe', 'Full name should be John Doe'
assert admin1.getFullName() == 'Monika Justin', 'Full name should be Monika Justin'
assert teamLead1.getFullName() == 'Michael Specter', 'Full name should be Michael Specter'
assert employee2.login() == 'sjane@mail.com has logged in'
assert employee2.addRequest() == 'Request has been added'
assert employee2.logout() == 'sjane@mail.com has logged out'

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMember():
    print(indiv_emp.getFullName())

assert admin1.addUser() == 'User has been added'

req2.set_status('closed')
print(req2.closeRequest())